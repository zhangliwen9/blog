class School < ApplicationRecord
  belongs_to :user
  has_many :teachers
  has_many :students
  has_many :books
end

# == Schema Information
#
# Table name: login_logs
#
#  id         :bigint(8)        not null, primary key
#  name       :string
#  lever      :integer
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  user_id    :bigint(10)
#
#
#
