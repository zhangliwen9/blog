class Teacher < ApplicationRecord
  has_many :students
end

# == Schema Information
#
# Table name: login_logs
#
#  id         :bigint(8)        not null, primary key
#  name       :string
#  lever      :integer
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  user_id    :bigint(8)
#
#
#
