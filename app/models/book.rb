class Book < ApplicationRecord
  belongs_to :student
  belongs_to :author
end

# == Schema Information
#
# Table name: login_logs
#
#  id         :bigint(8)        not null, primary key
#  name       :string		not null
#  author_id  :integer
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  user_id    :bigint(8)
#
#
#
