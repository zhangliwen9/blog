class Student < ApplicationRecord
  belongs_to :school
  belongs_to :teacher
  has_many :books
end

# == Schema Information
#
# Table name: login_logs
#
#  id         :bigint(8)        not null, primary key
#  name       :string
#  school_id  :integer		not null
#  teacher_id :integer
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  user_id    :bigint(10)
#
#
#
